﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Paolo.VideoPlayer;
using UnityEditor;
using UnityEngine.Video;

namespace Tests
{
    public class VideoPlayerTests
    {
        EditorVideoPlayer playerWindow;

        [SetUp]
        public void Init()
        {
            playerWindow = EditorVideoPlayer.ShowWindow();
        }

        [TearDown]
        public void Dispose()
        {
            playerWindow.Close();
        }

        [Test]
        public void CreateVideoPlayer_PlayerDoesNotExists_Creates()
        {
            Assert.IsNotNull(GameObject.Find("VideoPlayer"));
        }

        [UnityTest]
        public IEnumerator CreateVideoPlayer_PlayerAlreadyExists_Ignores()
        {
            EditorVideoPlayer anotherPlayerWindow = EditorVideoPlayer.ShowWindow();
            yield return null;
            VideoPlayer[] players = Resources.FindObjectsOfTypeAll<VideoPlayer>();
            Assert.IsTrue(players.Length == 1);
        }

        [UnityTest]
        public IEnumerator DisposeVideoPlayer_PlayerAlreadyExists_Success()
        {
            VideoPlayer[] players = Resources.FindObjectsOfTypeAll<VideoPlayer>();
            Assert.IsTrue(players.Length == 1);
            playerWindow.Close();
            yield return null;

            players = Resources.FindObjectsOfTypeAll<VideoPlayer>();
            Assert.IsTrue(players.Length == 0);
        }


        [Test]
        public void PlayVideo_EmptyPlaylist_DoesNotPlay()
        {
            Playlist playlist = GetEmptyPlaylist();
            EditorVideoPlayer.SetPlaylist(playlist);

            EditorVideoPlayer.PlayCurrent();

            VideoPlayer player = EditorVideoPlayer.GetVideoPlayer();
            Assert.IsFalse(player.isPlaying);
        }

        [UnityTest]
        public IEnumerator PlayVideo_FullPlaylist_Plays()
        {
            Playlist playlist = GetOneVideoPlaylist();
            EditorVideoPlayer.SetPlaylist(playlist);

            EditorVideoPlayer.PlayCurrent();
            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            VideoPlayer player = EditorVideoPlayer.GetVideoPlayer();
            Assert.IsTrue(player.isPlaying);
        }

        [UnityTest]
        public IEnumerator PauseVideo_VideoIsPlaying_Pauses()
        {
            Playlist playlist = GetOneVideoPlaylist();
            EditorVideoPlayer.SetPlaylist(playlist);

            EditorVideoPlayer.PlayCurrent();
            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            VideoPlayer player = EditorVideoPlayer.GetVideoPlayer();
            EditorVideoPlayer.Pause();
            Assert.IsFalse(player.isPlaying);
        }

        [UnityTest]
        public IEnumerator StopVideo_VideoIsPlaying_Stops()
        {
            Playlist playlist = GetOneVideoPlaylist();
            EditorVideoPlayer.SetPlaylist(playlist);

            EditorVideoPlayer.PlayCurrent();
            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            VideoPlayer player = EditorVideoPlayer.GetVideoPlayer();
            EditorVideoPlayer.Stop();
            Assert.IsFalse(player.isPlaying);
        }

        [UnityTest]
        public IEnumerator PlayNext_HasOneVideo_PlaysSame()
        {
            Playlist playlist = GetOneVideoPlaylist();
            EditorVideoPlayer.SetPlaylist(playlist);

            EditorVideoPlayer.PlayCurrent();
            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            VideoPlayer player = EditorVideoPlayer.GetVideoPlayer();
            string currentVideo = player.url;
            EditorVideoPlayer.PlayNext();

            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            Assert.AreEqual(currentVideo, player.url);
        }

        [UnityTest]
        public IEnumerator PlayNext_HasMoreVideos_PlaysNext()
        {
            Playlist playlist = GetTwoVideosPlaylist();
            EditorVideoPlayer.SetPlaylist(playlist);

            EditorVideoPlayer.PlayCurrent();
            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            VideoPlayer player = EditorVideoPlayer.GetVideoPlayer();
            string currentVideo = player.url;
            EditorVideoPlayer.PlayNext();

            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            Assert.AreNotEqual(currentVideo, player.url);
        }

        [UnityTest]
        public IEnumerator PlayPrevious_HasOneVideo_PlaysSame()
        {
            Playlist playlist = GetOneVideoPlaylist();
            EditorVideoPlayer.SetPlaylist(playlist);

            EditorVideoPlayer.PlayCurrent();
            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            VideoPlayer player = EditorVideoPlayer.GetVideoPlayer();
            string currentVideo = player.url;
            EditorVideoPlayer.PlayPrevious();

            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            Assert.AreEqual(currentVideo, player.url);
        }

        [UnityTest]
        public IEnumerator PlayPrevious_HasMoreVideos_PlaysPrevious()
        {
            Playlist playlist = GetTwoVideosPlaylist();
            EditorVideoPlayer.SetPlaylist(playlist);

            EditorVideoPlayer.PlayCurrent();
            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            VideoPlayer player = EditorVideoPlayer.GetVideoPlayer();
            string currentVideo = player.url;
            EditorVideoPlayer.PlayPrevious();

            for (int i = 0; i < 100; i++) { yield return null; } //wait for a while

            Assert.AreNotEqual(currentVideo, player.url);
        }

        static Playlist GetEmptyPlaylist()
        {
            return (ScriptableObject.CreateInstance(typeof(Playlist)) as Playlist);
        }

        static Playlist GetOneVideoPlaylist()
        {
            Playlist playlist = GetEmptyPlaylist();
            playlist.AddVideoFromPath("Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie1.mp4");
            return playlist;
        }

        static Playlist GetTwoVideosPlaylist()
        {
            Playlist playlist = GetOneVideoPlaylist();
            playlist.AddVideoFromPath("Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie2.mp4");
            return playlist;
        }
    }
}
