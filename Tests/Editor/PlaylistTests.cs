﻿using NUnit.Framework;
using Paolo.VideoPlayer;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using System.Linq;

namespace Tests
{
    public class PlaylistTests
    {
        [Test]
        public void GetNextVideo_EmptyPlaylist_ReturnsNoPath()
        {
            //Arrange
            Playlist playlist = ScriptableObject.CreateInstance<Playlist>();

            //Act
            int currentVideoIndex = -1;
            string videoPath = playlist.GetPathOfNextVideo(ref currentVideoIndex);

            //Assert
            Assert.IsTrue(videoPath == string.Empty);
        }

        [Test]
        public void GetPreviousVideo_EmptyPlaylist_ReturnsNoPath()
        {
            Playlist playlist = ScriptableObject.CreateInstance<Playlist>();

            int currentVideoIndex = 1;
            string videoPath = playlist.GetPathOfPreviousVideo(ref currentVideoIndex);

            Assert.IsTrue(videoPath == string.Empty);
        }

        [Test]
        public void GetNextVideo_NonEmptyPlaylist_ReturnsPath()
        {
            Playlist playlist = ScriptableObject.CreateInstance<Playlist>();
            string originalVideoPath = "Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie1.mp4";
            playlist.AddVideoFromPath(originalVideoPath);

            int currentVideoIndex = -1;
            string resultVideoPath = playlist.GetPathOfNextVideo(ref currentVideoIndex);
            Assert.AreEqual(GetVideoPathInPackagesFolder(originalVideoPath).Split('/').Last(), resultVideoPath.Split('/').Last());
        }

        [Test]
        public void GetPreviousVideo_NonEmptyPlaylist_ReturnsPath()
        {
            Playlist playlist = ScriptableObject.CreateInstance<Playlist>();
            string originalVideoPath = "Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie1.mp4";
            playlist.AddVideoFromPath(originalVideoPath);

            int currentVideoIndex = 1;
            string resultVideoPath = playlist.GetPathOfPreviousVideo(ref currentVideoIndex);
            Assert.AreEqual(GetVideoPathInPackagesFolder(originalVideoPath).Split('/').Last(), resultVideoPath.Split('/').Last());
        }

        [Test]
        public void GetNextVideo_CurrentVideoIsLast_ReturnsFirstVideo()
        {
            Playlist playlist = ScriptableObject.CreateInstance<Playlist>();
            string[] videoPaths = new string[]
            {
                "Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie1.mp4",
                "Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie2.mp4",
                "Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie3.mp4"
            };

            foreach (string videoPath in videoPaths)
            {
                playlist.AddVideoFromPath(videoPath);
            }
            int currentVideoIndex = videoPaths.Length - 1;
            string resultVideoPath = playlist.GetPathOfNextVideo(ref currentVideoIndex);
            Assert.AreEqual(GetVideoPathInPackagesFolder(videoPaths[0]).Split('/').Last(), resultVideoPath.Split('/').Last());
        }

        [Test]
        public void GetPreviousVideo_CurrentVideoIsFirst_ReturnsLastVideo()
        {
            Playlist playlist = ScriptableObject.CreateInstance<Playlist>();
            string[] videoPaths = new string[]
            {
                "Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie1.mp4",
                "Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie2.mp4",
                "Packages/com.paoloabela.editorvideoplayer/Editor/Videos/movie3.mp4"
            };

            foreach (string videoPath in videoPaths)
            {
                playlist.AddVideoFromPath(videoPath);
            }
            int currentVideoIndex = 0;
            string resultVideoPath = playlist.GetPathOfPreviousVideo(ref currentVideoIndex);
            Assert.AreEqual(GetVideoPathInPackagesFolder(videoPaths[2]).Split('/').Last(), resultVideoPath.Split('/').Last());
        }

        static string GetVideoPathInPackagesFolder(string originalVideoPath)
        {
            return Application.dataPath.Replace("Assets", string.Empty) + originalVideoPath.Replace("com.paoloabela.editorvideoplayer", "EditorVideoPlayer");
        }
    }
}
