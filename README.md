# Introduction

Hi! If you're using this package, you're probably the person evaluating my work. For this reason, I prepared this small readme that tells you how to properly setup the package. I hope you'll enjoy my efforts :)

# Setup

Add the following line to the dependencies, in the manifest.json of the project, to install the package:
"com.paoloabela.editorvideoplayer": "https://gitlab.com/rikudefuffs/editorvideoplayer.git"

Add the following line to the manifest.json of the project (or unit tests won't be displayed in the test runner)
"testables": ["com.paoloabela.editorvideoplayer"],

# Usage

If you're trying the tests: just run them from the Test Runner
If you're trying the real video player: 

- open it with CTRL + SHIFT + T (or go to Tools > Video Player)
- choose a playlist using the appropriate field in the UI (click it, then go to assets and choose one of the 3 playlists), or drag and drop a playlist ScriptableObject in the field
- press Play!
- Keep in mind that only one video player will be present at a time: if you try to open more of it, you'll simply get back to the current Video Player instance.

# Controls (intended behaviours)

- Play: plays current video
- Pause: pauses current video
- Stop: as pause, but also resets video time to 0
- PlayNext: goes to next video (or first one, if current video is the last one)
- PlayPrevious: goes to previous video (or last one, if current video is the first one)
- Loop (toggle): repeats the current video when it ends
- Loop playlist (toggle): repeats the playlist when the last video ends
- Mute volume (toggle): mutes/unmutes volume

# UI

UIElements were used to build the UI, along with USS. Both UXML templates and "simple" elements were used. The backend is provided by C# methods.

# Feedback

Feedback is much appreciated. I also left some comments in the UXML doc that ask for information that I didn't manage to find in the UIlements official documentation, so if you want to provide it feel free to do it!

Enjoy your testing!

Paolo