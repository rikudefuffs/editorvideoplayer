﻿namespace Paolo.VideoPlayer
{
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.Video;
    using UnityEngine.UIElements;
    using UnityEditor.UIElements;

    /// <summary>
    /// A video player that runs only in the Unity Editor
    /// </summary>
    public class EditorVideoPlayer : EditorWindow
    {
        const int NO_VIDEO_PLAYING = -1;
        const string USS_UNITY_TOGGLE_CHECKMARK = "unity-toggle__checkmark";
        const string USS_CUSTOM_PLAYERE_TOGGLE = "editorvideoplayer-toggle";

        static VideoPlayer player;
        static RenderTexture renderTexture;
        static GameObject playerInstance;
        static AudioSource playerAudioSource;

        static VisualElement root;
        static Image videoImage;
        static Slider currentVideoTimeSlider;
        static Label currentVideoTimeLabel;
        static Label totalVideoTimeLabel;

        static Playlist currentPlaylist;
        static string currentClipPath
        {
            get { return player.url; }
            set
            {
                if (string.IsNullOrEmpty(value)) { return; }
                player.url = value;
            }
        }
        static int currentClipIndex;
        static bool enableLoopOnTrackEnd;
        static bool isPlaylistLoopEnabled;
        static int lastSecondReached;

        [MenuItem("Tools/Video Player _%#T")] //CTRL + SHIFT + T
        public static EditorVideoPlayer ShowWindow()
        {
            return GetWindow<EditorVideoPlayer>(false, "Video Player", true);
        }

        void OnEnable()
        {
            SetupBackend();
            SetupFrontend();
        }

        void OnDisable()
        {
            if (!playerInstance) { return; }
            Stop();
            DestroyImmediate(playerInstance);
        }

        void Update()
        {
            if (!player || !player.isPlaying) { return; }
            videoImage.MarkDirtyRepaint();
            UpdateDurationUI();
        }

        static void UpdateDurationUI()
        {
            int currentSeconds = (int)player.time % 60;
            if (currentSeconds == lastSecondReached) { return; }
            int currentMinutes = Mathf.FloorToInt((int)player.time / 60);

            lastSecondReached = currentSeconds;
            currentVideoTimeLabel.text = string.Format("{0}:{1}", currentMinutes.ToString("00"), currentSeconds.ToString("00"));
            currentVideoTimeSlider.value = (float)(player.time / player.length);
        }

        #region Player Controls

        public static void PlayCurrent()
        {
            videoImage.MarkDirtyRepaint();
            player.Play();
        }

        public static void Pause()
        {
            player.Pause();
        }

        public static void Stop()
        {
            player.Stop();
            ClearOutRenderTexture(renderTexture);
            ResetTimeUI();
        }
        public static void PlayNext()
        {
            Pause();
            currentClipPath = currentPlaylist.GetPathOfNextVideo(ref currentClipIndex);
            PlayCurrent();
        }

        public static void PlayPrevious()
        {
            Pause();
            currentClipPath = currentPlaylist.GetPathOfPreviousVideo(ref currentClipIndex);
            PlayCurrent();
        }

        #endregion

        #region Setup

        static void SetupBackend()
        {
            if (!currentPlaylist)
            {
                currentPlaylist = GetDefaultEmptyPlaylist();
            }
            currentClipIndex = NO_VIDEO_PLAYING;

            if (playerInstance) { return; }
            renderTexture = Resources.Load<RenderTexture>("VideoPlayer");
            CreateVideoPlayer(out player);
            currentClipPath = currentPlaylist.GetPathOfNextVideo(ref currentClipIndex);
            enableLoopOnTrackEnd = player.isLooping;
            player.loopPointReached += OnVideoEnded;
            player.started += OnVideoStarted;
            lastSecondReached = 0;
        }

        void SetupFrontend()
        {
            root = rootVisualElement;
            root.styleSheets.Add(Resources.Load<StyleSheet>("EditorVideoPlayer_Style"));
            root.styleSheets.Add(Resources.Load<StyleSheet>("ToggleTemplate_Style"));

            VisualTreeAsset playerVisualTree = Resources.Load<VisualTreeAsset>("EditorVideoPlayer_Main");
            playerVisualTree.CloneTree(root);

            ClearOutRenderTexture(renderTexture);
            videoImage = root.Query<Image>();
            videoImage.image = renderTexture;
            videoImage.MarkDirtyRepaint();

            var playerButtons = root.Query<Button>();
            playerButtons.ForEach(SetupButton);

            ObjectField playlistField = root.Q<ObjectField>(null, "playlist-picker");
            playlistField.objectType = typeof(Playlist);
            playlistField.value = currentPlaylist;
            playlistField.RegisterCallback<ChangeEvent<Object>>(OnPlaylistChange);

            SetupLoopToggle();
            SetupCycleToggle();
            SetupVolumeToggle();

            SetupVideoTimeUI();
        }

        void SetupVideoTimeUI()
        {
            totalVideoTimeLabel = root.Q<Label>("TotalVideoTime");
            currentVideoTimeLabel = root.Q<Label>("CurrentVideoTime");
            currentVideoTimeSlider = root.Q<Slider>("VideoTimePercentage");
            currentVideoTimeSlider.SetEnabled(false);
            ResetTimeUI();
        }

        void SetupVolumeToggle()
        {
            Toggle toggle = root.Q<Toggle>("MuteVolume", USS_CUSTOM_PLAYERE_TOGGLE);
            toggle.value = false;
            toggle.RegisterValueChangedCallback(OnMuteSettingsChange);
            VisualElement toggleIcon = toggle.Q(null, USS_UNITY_TOGGLE_CHECKMARK);
            toggleIcon.style.backgroundImage = LoadIcon(toggle.name);
            toggle.tooltip = "Mute Volume";
        }

        void SetupCycleToggle()
        {
            Toggle toggle = root.Q<Toggle>("Cycle", USS_CUSTOM_PLAYERE_TOGGLE);
            toggle.value = false;
            toggle.RegisterValueChangedCallback(OnCycleSettingsChange);
            VisualElement toggleIcon = toggle.Q(null, USS_UNITY_TOGGLE_CHECKMARK);
            toggleIcon.style.backgroundImage = LoadIcon(toggle.name);
            toggle.tooltip = string.Format("{0} (Whole playlist)", toggle.name);
        }

        void SetupLoopToggle()
        {
            Toggle toggle = root.Q<Toggle>("Loop", USS_CUSTOM_PLAYERE_TOGGLE);
            toggle.value = player.isLooping;
            toggle.RegisterValueChangedCallback(OnLoopSettingsChange);
            VisualElement toggleIcon = toggle.Q(null, USS_UNITY_TOGGLE_CHECKMARK);
            toggleIcon.style.backgroundImage = LoadIcon(toggle.name);
            toggle.tooltip = string.Format("{0} (Single video)", toggle.name);
        }

        void SetupButton(Button button)
        {
            string buttonName = button.parent.name;
            VisualElement buttonIcon = button.Q(null, "editorvideoplayer-button-icon");
            buttonIcon.style.backgroundImage = LoadIcon(buttonName);

            System.Action callback = null;
            switch (buttonName)
            {
                case "Play": callback = PlayCurrent; break;
                case "Pause": callback = Pause; break;
                case "Stop": callback = Stop; break;
                case "Previous": callback = PlayPrevious; break;
                case "Next": callback = PlayNext; break;
            }
            button.clickable.clicked += callback;
            button.tooltip = buttonName;
        }
        static void CreateVideoPlayer(out VideoPlayer player)
        {
            playerInstance = new GameObject("VideoPlayer");
            playerInstance.hideFlags = HideFlags.HideAndDontSave;
            player = playerInstance.AddComponent<VideoPlayer>();
            player.renderMode = VideoRenderMode.RenderTexture;
            player.targetTexture = renderTexture;
            player.isLooping = false;
            player.aspectRatio = VideoAspectRatio.FitInside;
            player.audioOutputMode = VideoAudioOutputMode.AudioSource;
            playerAudioSource = player.gameObject.AddComponent<AudioSource>();
            player.SetTargetAudioSource(0, playerAudioSource);
        }

        #endregion

        #region Event Callbacks

        static void OnVideoStarted(VideoPlayer videoPlayer)
        {
            int mins = Mathf.FloorToInt((int)player.length / 60);
            int secs = (int)player.length % 60;
            totalVideoTimeLabel.text = mins.ToString("00") + ":" + secs.ToString("00");
        }

        static void OnVideoEnded(VideoPlayer videoPlayer)
        {
            if (enableLoopOnTrackEnd && player.isLooping) { return; }
            player.isLooping = enableLoopOnTrackEnd;
            if (enableLoopOnTrackEnd)
            {
                PlayCurrent();
                return;
            }
            bool isLastVideoOfPlaylist = currentPlaylist.CurrentVideoIsLastVideo(currentClipIndex);
            if (!isLastVideoOfPlaylist)
            {
                PlayNext();
                return;
            }
            if (isPlaylistLoopEnabled)
            {
                PlayNext();
                return;
            }
            Stop();
            ResetPlayerToBeginning();
        }

        void OnPlaylistChange(ChangeEvent<Object> newPlaylistEvent)
        {
            SetPlaylist(newPlaylistEvent.newValue as Playlist);
        }

        void OnLoopSettingsChange(ChangeEvent<bool> newEvent)
        {
            enableLoopOnTrackEnd = newEvent.newValue;
        }

        void OnCycleSettingsChange(ChangeEvent<bool> newEvent)
        {
            isPlaylistLoopEnabled = newEvent.newValue;
        }

        void OnMuteSettingsChange(ChangeEvent<bool> newEvent)
        {
            playerAudioSource.mute = newEvent.newValue;
        }

        #endregion

        #region Player Helpers

        static void ResetTimeUI()
        {
            currentVideoTimeSlider.value = 0;
            ResetTimeLabel(currentVideoTimeLabel);
            ResetTimeLabel(totalVideoTimeLabel);
        }

        static void ResetTimeLabel(Label label)
        {
            label.text = "00:00";
        }


        static void ResetPlayerToBeginning()
        {
            currentClipIndex = 0;
        }

        static Playlist GetDefaultEmptyPlaylist()
        {
            Playlist p = CreateInstance<Playlist>();
            p.name = "Empty";
            return p;
        }

        public static VideoPlayer GetVideoPlayer() { return player; }
        public static void SetPlaylist(Playlist newPlaylist)
        {
            if (!newPlaylist)
            {
                newPlaylist = GetDefaultEmptyPlaylist();
            }
            if (newPlaylist == currentPlaylist) { return; }

            currentPlaylist = newPlaylist;
            currentClipIndex = NO_VIDEO_PLAYING;

            if (!currentPlaylist) { return; }
            Stop();
            currentClipPath = currentPlaylist.GetPathOfNextVideo(ref currentClipIndex);
        }
        #endregion

        #region General Helpers

        static void ClearOutRenderTexture(RenderTexture renderTexture)
        {
            RenderTexture temp = RenderTexture.active;
            RenderTexture.active = renderTexture;
            GL.Clear(true, true, Color.black);
            RenderTexture.active = temp;
        }

        static Texture2D LoadIcon(string iconName)
        {
            return Resources.Load<Texture2D>("Icons/" + iconName);
        }

        #endregion
    }
}