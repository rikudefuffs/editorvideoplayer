﻿namespace Paolo.VideoPlayer
{
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.Video;

    ///<summary>
    /// The representation of a list of videos that can be played by the VideoPlayer
    ///</summary>
    [CreateAssetMenu(fileName = "Playlist", menuName = "Custom/Playlist")]
    public class Playlist : ScriptableObject
    {
        [SerializeField]
        List<VideoClip> videos;

        public Playlist()
        {
            videos = new List<VideoClip>();
        }

        public string GetPathOfNextVideo(ref int currentVideoIndex)
        {
            return GetPathOfVideo(true, ref currentVideoIndex);
        }

        public string GetPathOfPreviousVideo(ref int currentVideoIndex)
        {
            return GetPathOfVideo(false, ref currentVideoIndex);
        }

        string GetPathOfVideo(bool lookForNextOne, ref int currentVideoIndex)
        {
            if (videos.Count < 1)
            {
                Debug.Log("Playlist is empty, pick one from Assets"); //throw new System.Exception("Playlist is empty");
                currentVideoIndex = -1;
                return string.Empty;
            }
            if (lookForNextOne)
            {
                currentVideoIndex = Modulo(currentVideoIndex + 1, videos.Count);
            }
            else
            {
                currentVideoIndex = Modulo(currentVideoIndex - 1, videos.Count);
            }
            string result = videos[currentVideoIndex].originalPath;
            return result;
        }

        public bool CurrentVideoIsLastVideo(int videoIndex)
        {
            return videoIndex == (videos.Count - 1);
        }

        public static int Modulo(int value, int lenght)
        {
            return ((value % lenght) + lenght) % lenght;
        }

        public void AddVideoFromPath(string videoPath)
        {
            VideoClip clip = AssetDatabase.LoadAssetAtPath(videoPath, typeof(VideoClip)) as VideoClip;
            videos.Add(clip);
        }

        public int GetVideosCount() { return videos.Count; }
    }
}